package ru.t1.ytarasov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.exception.AbstractException;

public interface IProjectTaskService {

    void bindTaskToProject(@Nullable String userId, @Nullable String taskId, @Nullable String projectId) throws AbstractException;

    void removeProjectById(@Nullable String userId, @Nullable String projectId) throws AbstractException;

    void removeProjectByIndex(@Nullable String userId, int projectIndex) throws AbstractException;

    void clearAllProjects(@Nullable String userId) throws AbstractException;

    void unbindTaskFromProject(@Nullable String userId, @Nullable String taskId, @Nullable String projectId) throws AbstractException;

}
