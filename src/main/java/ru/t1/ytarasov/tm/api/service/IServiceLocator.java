package ru.t1.ytarasov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IAuthService getAuthService();

}
