package ru.t1.ytarasov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.exception.AbstractException;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "version";

    @NotNull
    public static final String ARGUMENT = "-v";

    @NotNull
    public static final String DESCRIPTION = "Show application version";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[VERSION]");
        System.out.println("1.23.0");
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
