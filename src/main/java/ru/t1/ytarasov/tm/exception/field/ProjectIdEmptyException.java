package ru.t1.ytarasov.tm.exception.field;

public final class ProjectIdEmptyException extends AbstractFieldException {

    public ProjectIdEmptyException() {
        super("Error! Project ID is empty...");
    }

}
