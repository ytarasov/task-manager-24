package ru.t1.ytarasov.tm;

import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.component.Bootstrap;

public class Application {

    public static void main(String[] args) {
        final @NotNull Bootstrap bootstrap = new Bootstrap();
        bootstrap.runArguments(args);
    }

}
