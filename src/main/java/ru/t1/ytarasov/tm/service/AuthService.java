package ru.t1.ytarasov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.service.IAuthService;
import ru.t1.ytarasov.tm.api.service.IUserService;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.exception.field.LoginEmptyException;
import ru.t1.ytarasov.tm.exception.field.PasswordEmptyException;
import ru.t1.ytarasov.tm.exception.user.AccessDeniedException;
import ru.t1.ytarasov.tm.exception.user.IncorrectLoginOrPasswordException;
import ru.t1.ytarasov.tm.exception.user.PermissionException;
import ru.t1.ytarasov.tm.model.User;
import ru.t1.ytarasov.tm.util.HashUtil;

import java.util.Arrays;

public final class AuthService implements IAuthService {

    @NotNull private final IUserService userService;

    @Nullable private String userId;

    public AuthService(final @NotNull IUserService userService) {
        this.userService = userService;
    }

    @NotNull
    @Override
    public User registry(@Nullable final String login,
                         @Nullable final String password,
                         @Nullable final String email
    ) throws AbstractException {
        return userService.create(login, password, email);
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new IncorrectLoginOrPasswordException();
        if (user.isLocked()) throw new IncorrectLoginOrPasswordException();
        @NotNull final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new IncorrectLoginOrPasswordException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @NotNull
    @Override
    public String getUserId() throws AbstractException {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @NotNull
    @Override
    public User getUser() throws AbstractException {
        if (!isAuth()) throw new AccessDeniedException();
        final User user = userService.findOneById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

    @Override
    public void checkRoles(final @Nullable Role @Nullable [] roles) throws AbstractException {
        if (roles == null) return;
        @Nullable final User user = getUser();
        @Nullable final Role role = user.getRole();
        if (role == null) return;
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}
