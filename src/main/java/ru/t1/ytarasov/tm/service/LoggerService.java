package ru.t1.ytarasov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.service.ILoggerService;

import java.io.IOException;
import java.util.logging.*;

public final class LoggerService implements ILoggerService {

    @NotNull private static final String CONFIG_FILE = "/logger.properties";

    @NotNull private static final String COMMANDS = "COMMANDS";

    @NotNull private static final String COMMANDS_FILE = "./commands.xml";

    @NotNull private static final String ERRORS = "ERRORS";

    @NotNull private static final String ERRORS_FILE = "./errors.xml";

    @NotNull private static final String MESSAGES = "MESSAGES";

    @NotNull private static final String MESSAGES_FILE = "./messages.xml";

    @NotNull private static final LogManager MANAGER = LogManager.getLogManager();

    @NotNull private static final Logger LOGGER_ROOT = getLoggerRoot();

    @NotNull private static final Logger LOGGER_COMMANDS = getLoggerCommands();

    @NotNull private static final Logger LOGGER_ERRORS = getLoggerErrors();

    @NotNull private static final Logger LOGGER_MESSAGES = getLoggerMessages();

    @NotNull private final ConsoleHandler consoleHandler = getConsoleHandler();

    {
        init();
        registry(LOGGER_COMMANDS, COMMANDS_FILE, false);
        registry(LOGGER_ERRORS, ERRORS_FILE, true);
        registry(LOGGER_MESSAGES, MESSAGES_FILE, true);
    }

    @NotNull
    private static Logger getLoggerRoot() {
        return Logger.getLogger("");
    }

    @NotNull
    private static Logger getLoggerCommands() {
        return Logger.getLogger(COMMANDS);
    }

    @NotNull
    private static Logger getLoggerErrors() {
        return Logger.getLogger(ERRORS);
    }

    @NotNull
    private static Logger getLoggerMessages() {
        return Logger.getLogger(MESSAGES);
    }

    private void init() {
        try {
            MANAGER.readConfiguration(LoggerService.class.getResourceAsStream(CONFIG_FILE));
        } catch (final IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    @NotNull
    private ConsoleHandler getConsoleHandler() {
        @NotNull ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage();
            }
        });
        return handler;
    }

    private void registry(@NotNull final Logger logger, @Nullable final String fileName, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(fileName));
        } catch (final IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    @Override
    public void info(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_MESSAGES.info(message);
    }

    @Override
    public void debug(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_MESSAGES.fine(message);
    }

    @Override
    public void command(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_COMMANDS.info(message);
    }

    @Override
    public void error(@Nullable final Exception e) {
        if (e == null) return;
        LOGGER_ERRORS.log(Level.SEVERE, e.getMessage(), e);
    }

}
